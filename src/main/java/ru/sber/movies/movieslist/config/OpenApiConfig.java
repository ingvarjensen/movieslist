package ru.sber.movies.movieslist.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI libraryProject() {
        return new OpenAPI()
                .info(new Info()
                        .title("API Онлайн Фильмотеки")
                        .description("Сервис аренды книг онлайн")
                        .version("1.0")
                        .license(new License().name("Apache 2.0").url("http://sringdoc.org"))
                        .contact(new Contact().name("Andrei A. Gavrilov")
                                .email("myemail@course.com"))
                );
    }
}
