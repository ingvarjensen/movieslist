package ru.sber.movies.movieslist.model;

public enum Genre {
    FANTASY,
    SCIENCE_FICTION,
    THRILLER,
    HORROR,
    COMEDY,
    ACTION_MOVIE,
    HISTORICAL,
    DOCUMENTARY
}
