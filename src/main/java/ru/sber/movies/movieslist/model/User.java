package ru.sber.movies.movieslist.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String login;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String phoneNumber;
    private String address;
    private String email;
    private LocalDate dateCreated;
    @ManyToOne
    @JoinColumn (name = "role_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ROLE_ID"))
    private Role roleId;
    @OneToMany(mappedBy = "user")
    private List<Order> orderList;
}
