package ru.sber.movies.movieslist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieslistApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieslistApplication.class, args);
	}

}
