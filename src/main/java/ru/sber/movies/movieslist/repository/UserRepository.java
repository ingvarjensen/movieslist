package ru.sber.movies.movieslist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.movies.movieslist.model.Film;
import ru.sber.movies.movieslist.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(nativeQuery = true,
            value = """
                    select * from users where title = :email
                    """
    )
    User getUserByEmail(String email);
}
