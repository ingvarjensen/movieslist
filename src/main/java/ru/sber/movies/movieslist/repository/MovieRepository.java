package ru.sber.movies.movieslist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.movies.movieslist.model.Film;

@Repository
public interface MovieRepository extends JpaRepository<Film, Long> {

    @Query(nativeQuery = true,
            value = """
                    select * from films where title = :name
                    """
    )
    Film getFilmByName(String name);
}
