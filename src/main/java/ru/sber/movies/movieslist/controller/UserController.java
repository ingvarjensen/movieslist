package ru.sber.movies.movieslist.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.movies.movieslist.exception.NotFoundException;
import ru.sber.movies.movieslist.model.User;
import ru.sber.movies.movieslist.repository.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Tag(name = "Users")
public class UserController {
    private final UserRepository userRepository;

    @Operation(description = "List all users")
    @GetMapping("/list")
    public ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.accepted().body(userRepository.findAll());
    }

    //localhost:8080/movies/getOne?UserId=1
    @GetMapping("/getOne")
    public ResponseEntity<User> getOneUser(@RequestParam(value = "userId") Long movieId){
        return ResponseEntity.accepted().body(userRepository.findById(movieId).orElseThrow(() -> new NotFoundException("User not found with this ID")));
    }

    @PostMapping("/create")
    public ResponseEntity<User> create(@RequestBody User user){
        return ResponseEntity.accepted().body(userRepository.save(user));
    }

    @PutMapping("/update")
    public ResponseEntity<User> update(@RequestBody User user, @RequestParam(value = "id") Long id){
        User foundUser =userRepository.findById(id).orElseThrow();
        foundUser.setLogin(user.getLogin());
        foundUser.setPassword(user.getPassword());
        foundUser.setFirstName(user.getFirstName());
        foundUser.setMiddleName(user.getMiddleName());
        foundUser.setLastName(user.getLastName());
        foundUser.setDateOfBirth(user.getDateOfBirth());
        foundUser.setPhoneNumber(user.getPhoneNumber());
        foundUser.setAddress(user.getAddress());
        foundUser.setEmail(user.getEmail());
        return ResponseEntity.accepted().body(userRepository.save(foundUser));
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam(value = "id") Long id){
        userRepository.deleteById(id);
    }

}
