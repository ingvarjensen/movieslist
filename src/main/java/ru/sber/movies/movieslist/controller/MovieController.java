package ru.sber.movies.movieslist.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.movies.movieslist.exception.NotFoundException;
import ru.sber.movies.movieslist.model.Film;
import ru.sber.movies.movieslist.repository.MovieRepository;

import java.util.List;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor
@Tag(name = "Movies")
public class MovieController {
    private final MovieRepository movieRepository;

    @Operation(description = "List all movies")
    @GetMapping("/list")
    public ResponseEntity<List<Film>> getAll(){
        return ResponseEntity.accepted().body(movieRepository.findAll());
    }

    //localhost:8080/movies/getOne?movieId=1
    @GetMapping("/getOne")
    public ResponseEntity<Film> getOne(@RequestParam(value = "movieId") Long movieId){
        return ResponseEntity.accepted().body(movieRepository.findById(movieId).orElseThrow(() -> new NotFoundException("Film not found with this ID")));
    }

    @PostMapping("/create")
    public ResponseEntity<Film> create(@RequestBody Film film){
        return ResponseEntity.accepted().body(movieRepository.save(film));
    }

    @PutMapping("/update")
    public ResponseEntity<Film> update(@RequestBody Film film, @RequestParam(value = "id") Long id){
        Film foundFilm =movieRepository.findById(id).orElseThrow();
        foundFilm.setCountry(film.getCountry());
        foundFilm.setGenre(film.getGenre());
        foundFilm.setTitle(film.getTitle());
        foundFilm.setPremierYear(film.getPremierYear());
        return ResponseEntity.accepted().body(movieRepository.save(foundFilm));
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam(value = "id") Long id){
       movieRepository.deleteById(id);
    }

}
